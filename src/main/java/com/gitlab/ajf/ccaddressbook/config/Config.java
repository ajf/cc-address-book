package com.gitlab.ajf.ccaddressbook.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.ajf.ccaddressbook.model.AddressBook;
import com.gitlab.ajf.ccaddressbook.model.Contact;
import com.gitlab.ajf.ccaddressbook.repository.impl.FileBackedStorage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Config {

    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                // Limit Swagger docs to our stuff.
                .paths(PathSelectors.regex("/v1/.*"))
                .build();
    }

    @Bean
    public FileBackedStorage<String, Contact> contactStorage(@Value("${storageLocation.contacts:storage/contacts.json}") String location,
                                                             ObjectMapper objectMapper) {
        return new FileBackedStorage<>(location, objectMapper, String.class, Contact.class);
    }

    @Bean
    public FileBackedStorage<String, AddressBook> addressBookStorage(@Value("${storageLocation.addressBooks:storage/addressBooks.json}") String location,
                                                                     ObjectMapper objectMapper) {
        return new FileBackedStorage<>(location, objectMapper, String.class, AddressBook.class);
    }

}
