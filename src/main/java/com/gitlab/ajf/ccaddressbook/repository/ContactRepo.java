package com.gitlab.ajf.ccaddressbook.repository;

import com.gitlab.ajf.ccaddressbook.model.Contact;

import java.util.List;

public interface ContactRepo {

    /**
     * @return A list of all contacts, sorted by name in alphabetical order.
     */
    List<Contact> getAll();

    /**
     * Saves a contact, either creating a new one, or updating an existing one.
     * @param contact Contact to save
     */
    void save(Contact contact);

    /**
     *
     * @param name Name of contact to retrieve
     * @return The requested contact
     */
    Contact get(String name);
}
