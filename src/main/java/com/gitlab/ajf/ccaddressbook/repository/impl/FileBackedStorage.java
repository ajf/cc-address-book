package com.gitlab.ajf.ccaddressbook.repository.impl;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;

public class FileBackedStorage<K, V> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileBackedStorage.class);
    private final String storageLocation;
    private final ObjectMapper objectMapper;
    private final Class<K> keyClass;
    private final Class<V> dataClass;

    /**
     *
     * @param storageLocation Path to file storage
     * @param objectMapper Jackson ObjectMapper
     * @param keyClass Class for the key for the stored map.
     * @param dataClass Class for the key for the stored values
     */
    public FileBackedStorage(String storageLocation, ObjectMapper objectMapper, Class<K> keyClass, Class<V> dataClass) {
        this.storageLocation = storageLocation;
        this.objectMapper = objectMapper;
        // These two are required to make our life easier when deserialising the stored data in one place.
        // Easier methods might have been:
        // 1. Duplicate the code in both Repos
        // 2. Create a Wrapper class that can be passed along to the objectMapper when reading the value
        // 3. Use a DAO layer and a light database
        this.keyClass = keyClass;
        this.dataClass = dataClass;
    }

    /**
     * Loads data from provided storage location, if available.
     * If unavailable, returns Optional.empty()
     * @return Requested data if available, empty otherwise.
     */
    public Optional<Map<K,V>> loadData() {
        Optional<Map<K,V>> result = Optional.empty();
        if (Files.isRegularFile(Paths.get(storageLocation))) {
            File storage = Paths.get(storageLocation).toFile();
            try {
                LOGGER.info("Loading storage: {}", storageLocation);
                JavaType type = objectMapper.getTypeFactory().constructParametricType(Map.class, keyClass, dataClass);
                result = Optional.of(objectMapper.readValue(storage, type));
            } catch (IOException e) {
                LOGGER.error("Unable to load storage file {}", storageLocation, e);
            }
        } else {
            LOGGER.debug("Unable to load storage {}", storageLocation);
        }
        return result;
    }

    /**
     * Saves data to provided location
     * @param data Data to save.
     */
    public void saveData(Map<K,V> data) {
        File fileName = new File(storageLocation);
        try (FileWriter file = new FileWriter(fileName)) {
            file.write(objectMapper.writeValueAsString(data));
            LOGGER.debug("Wrote data to file {}", fileName.getAbsoluteFile());
        } catch (IOException e) {
            LOGGER.error("Unable to write to storage location {}", storageLocation, e);
        }
    }
}
