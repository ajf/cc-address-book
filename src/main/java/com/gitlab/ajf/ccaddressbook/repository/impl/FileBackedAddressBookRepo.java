package com.gitlab.ajf.ccaddressbook.repository.impl;

import com.gitlab.ajf.ccaddressbook.exception.AddressBookNotFoundException;
import com.gitlab.ajf.ccaddressbook.model.AddressBook;
import com.gitlab.ajf.ccaddressbook.model.Contact;
import com.gitlab.ajf.ccaddressbook.repository.AddressBookRepo;
import com.gitlab.ajf.ccaddressbook.repository.ContactRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Service
public class FileBackedAddressBookRepo implements AddressBookRepo {

    private final FileBackedStorage<String, AddressBook> storage;
    private final Map<String, AddressBook> addressBooks;
    private final ContactRepo contacts;

    @Autowired
    public FileBackedAddressBookRepo(FileBackedStorage<String, AddressBook> storage, ContactRepo contacts) {
        this.storage = storage;
        addressBooks = storage.loadData().orElseGet(HashMap::new);
        this.contacts = contacts;
    }

    @Override
    public Set<AddressBook> getAll() {
        Set<AddressBook> result = new HashSet<>();
        addressBooks.values().forEach(x -> result.add(new AddressBook(x.getName(), Set.copyOf(x.getContacts()))));
        return result;
    }

    @Override
    public void save(AddressBook addressBook) {
        addressBooks.put(addressBook.getName(), addressBook);
        // Ensure AddressBook and Contacts are up-to-date
        // In the current model this *is* doubling up on disk usage.
        // But for simplicity of the application we'll leave it as is for now.
        // Ideally, we would want to just link them via name/id.
        for (Contact c: addressBook.getContacts()) {
            contacts.save(c);
        }
        storage.saveData(Map.copyOf(addressBooks));
    }

    @Override
    public AddressBook get(String name) throws AddressBookNotFoundException {
        AddressBook addressBook = addressBooks.get(name);
        if (addressBook != null) {
            // Doesn't do a deep copy on the contacts,
            return new AddressBook(addressBook.getName(), Set.copyOf(addressBook.getContacts()));
        }
        throw new AddressBookNotFoundException();
    }
}
