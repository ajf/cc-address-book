package com.gitlab.ajf.ccaddressbook.repository;

import com.gitlab.ajf.ccaddressbook.exception.AddressBookNotFoundException;
import com.gitlab.ajf.ccaddressbook.model.AddressBook;

import java.util.Set;

public interface AddressBookRepo {

    /**
     * @return A set of all available address books.
     */
    Set<AddressBook> getAll();

    /**
     * Saves an address book - will create or update (replace) an existing one,
     * depending on if one with the same name exists already.
     * @param addressBook Address book to save
     *
     */
    void save(AddressBook addressBook);

    /**
     * @param name Name of the address book to retrieve
     * @return The specific address book if found.
     */
    AddressBook get(String name) throws AddressBookNotFoundException;
}
