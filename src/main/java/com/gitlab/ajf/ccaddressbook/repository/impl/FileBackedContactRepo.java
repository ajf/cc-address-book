package com.gitlab.ajf.ccaddressbook.repository.impl;

import com.gitlab.ajf.ccaddressbook.exception.ContactNotFoundException;
import com.gitlab.ajf.ccaddressbook.model.Contact;
import com.gitlab.ajf.ccaddressbook.repository.ContactRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class FileBackedContactRepo implements ContactRepo {

    private final FileBackedStorage<String, Contact> storage;
    private final Map<String, Contact> contacts;

    @Autowired
    public FileBackedContactRepo(FileBackedStorage<String, Contact> contactStorage) {
        this.storage = contactStorage;
        contacts = storage.loadData().orElseGet(HashMap::new);
    }

    @Override
    public List<Contact> getAll() {
        // Create a copy of all contacts
        List<Contact> result = new ArrayList<>();
        contacts.values().forEach(x -> result.add(new Contact(x.getName(), x.getPhoneNumber())));
        return result.stream().sorted(Comparator.comparing(Contact::getName)).collect(Collectors.toList());
    }

    @Override
    public void save(Contact contact) {
        contacts.put(contact.getName(), contact);
        storage.saveData(Map.copyOf(contacts));
    }

    @Override
    public Contact get(String name) {
        if (contacts.containsKey(name)) {
            Contact contact = contacts.get(name);
            // Prevent passing the "saved" one back, create a copy of the existing one.
            return new Contact(contact.getName(), contact.getPhoneNumber());
        } else {
            throw new ContactNotFoundException();
        }
    }


}
