package com.gitlab.ajf.ccaddressbook.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "address book not found")
public class AddressBookNotFoundException extends RuntimeException {
}
