package com.gitlab.ajf.ccaddressbook.controller;

import com.gitlab.ajf.ccaddressbook.model.AddressBook;
import com.gitlab.ajf.ccaddressbook.model.Contact;
import com.gitlab.ajf.ccaddressbook.repository.AddressBookRepo;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/v1/address-book")
public class AddressBookController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AddressBookController.class);
    private final AddressBookRepo repo;

    @Autowired
    public AddressBookController(AddressBookRepo repo) {
        this.repo = repo;
    }

    @RequestMapping(method = RequestMethod.POST)
    public void saveAddressBook(@RequestBody AddressBook addressBook) {
        LOGGER.debug("Saving address book: {}", addressBook);
        repo.save(addressBook);
    }

    @RequestMapping(path = "/{addressBook}", method = RequestMethod.GET)
    public AddressBook getAddressBook(@PathVariable("addressBook") String addressBook) {
        LOGGER.debug("Retrieving address book {}", addressBook);
        return repo.get(addressBook);
    }


    @RequestMapping(method = RequestMethod.GET)
    public Set<AddressBook> getAddressBooks() {
        return repo.getAll();
    }

    /**
     * Compares two saved address books, returning the Disjunction/Symmetrical Difference.
     * @param firstBook First book to compare
     * @param secondBook Second book to compare
     * @return Contacts that only appear in one of the address books
     */
    @RequestMapping(path = "/compare/{firstBook}/{secondBook}", method = RequestMethod.GET)
    public Set<Contact> compareAddressBooks(@PathVariable("firstBook") String firstBook,
                                            @PathVariable("secondBook") String secondBook) {
        AddressBook first = repo.get(firstBook);
        AddressBook second = repo.get(secondBook);

        Collection<Contact> disjunction = CollectionUtils.disjunction(first.getContacts(), second.getContacts());

        return Set.copyOf(disjunction);
    }

    /**
     * Compares two saved address books, returning the Disjunction/Symmetrical Difference.
     * @return Contacts that only appear in one of the address books
     */
    @RequestMapping(path = "/compare", method = RequestMethod.POST)
    public Set<Contact> compareProvidedAddressBooks(@RequestBody List<AddressBook> providedAddressBooks) {
        if (providedAddressBooks.size() != 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Need exactly two address books to find the disjunction");
        }
        Collection<Contact> disjunction = CollectionUtils.disjunction(providedAddressBooks.get(0).getContacts(),
                providedAddressBooks.get(1).getContacts());

        return Set.copyOf(disjunction);
    }

}
