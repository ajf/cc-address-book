package com.gitlab.ajf.ccaddressbook.controller;

import com.gitlab.ajf.ccaddressbook.model.Contact;
import com.gitlab.ajf.ccaddressbook.repository.ContactRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/contacts")
public class ContactController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContactController.class);

    private final ContactRepo repo;

    @Autowired
    public ContactController(ContactRepo repo) {
        this.repo = repo;
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addContact(@RequestBody Contact contact) {
        LOGGER.debug("Saving new contact {}", contact);
        repo.save(contact);
    }

    @RequestMapping(path = "/{contact}", method = RequestMethod.GET)
    public Contact getContact(@PathVariable("contact") String contact) {
        LOGGER.debug("Retrieving Contact {}", contact);
        return repo.get(contact);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Contact> getContact() {
        LOGGER.debug("Retrieving all contacts");
        return repo.getAll();
    }
}
