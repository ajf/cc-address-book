package com.gitlab.ajf.ccaddressbook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CcAddressBookApplication {

    public static void main(String[] args) {
        SpringApplication.run(CcAddressBookApplication.class, args);
    }

}
