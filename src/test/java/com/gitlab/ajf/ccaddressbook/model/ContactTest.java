package com.gitlab.ajf.ccaddressbook.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ContactTest {

    @Test
    void contactEquality() {
        Contact sooty = new Contact("Sooty", "1234");
        Contact sweep1 = new Contact("Sweep", "1234");
        Contact sweep2 = new Contact("Sweep", "4321");

        assertNotEquals(sooty, sweep1);
        assertEquals(sooty, sooty);
        // As we're treating the "name" as the unique field,
        // we should consider two contacts with the same name to be the same contact.
        assertEquals(sweep1, sweep2);
    }
}