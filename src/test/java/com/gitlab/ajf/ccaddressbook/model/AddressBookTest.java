package com.gitlab.ajf.ccaddressbook.model;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class AddressBookTest {

    @Test
    void compareEquality() {
        AddressBook work1 = new AddressBook("Work", Set.of(new Contact("Homer", "1234")));
        AddressBook work2 = new AddressBook("Work", Set.of(new Contact("Carl", "4321")));
        AddressBook home = new AddressBook("Home", Set.of(new Contact("Marge", "6789")));

        // As we're treating the "name" as the unique field,
        // we should consider two address books with the same name to be the same.
        // Ideally, there should only exist one copy of a named address book inside the system.
        assertEquals(work1, work2);
        assertNotEquals(home, work1);
        assertEquals(home, home);
    }
}