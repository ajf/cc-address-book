package com.gitlab.ajf.ccaddressbook.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.ajf.ccaddressbook.exception.AddressBookNotFoundException;
import com.gitlab.ajf.ccaddressbook.model.AddressBook;
import com.gitlab.ajf.ccaddressbook.model.Contact;
import com.gitlab.ajf.ccaddressbook.repository.ContactRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class FileBackedAddressBookRepoTest {

    @Mock
    private FileBackedStorage<String, AddressBook> addressBookFileBackedStorage;
    @Mock
    private ContactRepo contactRepo;
    @Captor
    private ArgumentCaptor<Map<String, AddressBook>> captor;

    private FileBackedAddressBookRepo repo;

    @BeforeEach
    void setUp() {
        repo = new FileBackedAddressBookRepo(addressBookFileBackedStorage, contactRepo);
    }

    @Test
    void shouldSaveAndRetrieveAddressBook() {
        AddressBook ab = new AddressBook("Work", Set.of(new Contact("Frank", "1234")));
        repo.save(ab);
        verify(addressBookFileBackedStorage).saveData(captor.capture());

        assertTrue(captor.getValue().containsKey("Work"));
        assertEquals(ab.getContacts(), captor.getValue().get("Work").getContacts());
    }

    @Test
    void shouldFailLoadingAddressBook() {
        assertThrows(AddressBookNotFoundException.class, () -> repo.get(UUID.randomUUID().toString()));
    }

    @Test
    void shouldUpdateExistingAddressBook() {
        Contact frank = new Contact("Frank", "1234");
        Contact sue = new Contact("Sue", "4321");
        AddressBook ab = new AddressBook("Work", Set.of(frank));
        repo.save(ab);
        AddressBook firstRepoAb = repo.get("Work");
        ab.setContacts(Set.of(sue));
        repo.save(ab);
        AddressBook secondRepoAb = repo.get("Work");


        assertEquals(1, firstRepoAb.getContacts().size());
        assertEquals(1, secondRepoAb.getContacts().size());
        assertTrue(firstRepoAb.getContacts().contains(frank));
        assertTrue(secondRepoAb.getContacts().contains(sue));
    }

    @Test
    void shouldListAllAddressBooks() {
        AddressBook work = new AddressBook("Work", Set.of(new Contact("Frank", "1234")));
        AddressBook home = new AddressBook("Home", Set.of(new Contact("Sue", "4321")));

        repo.save(work);
        repo.save(home);

        Set<AddressBook> all = repo.getAll();

        assertEquals(2, all.size());
        assertEquals(Set.of(work, home), all);
    }

    @Test
    void shouldLoadAddressBookFromFile() throws URISyntaxException {
        String testFilePath = Paths.get(ClassLoader.getSystemResource("test_addressBook.json").toURI()).toAbsolutePath().toString();
        FileBackedAddressBookRepo fileBackedAddressBookRepo = new FileBackedAddressBookRepo(new FileBackedStorage<>(testFilePath, new ObjectMapper(), String.class, AddressBook.class), contactRepo);

        AddressBook work = fileBackedAddressBookRepo.get("Work");

        assertEquals(1, work.getContacts().size());
        Contact contact = work.getContacts().iterator().next();
        assertEquals("Jane", contact.getName());
        assertEquals("1234", contact.getPhoneNumber());
    }
}