package com.gitlab.ajf.ccaddressbook.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.ajf.ccaddressbook.model.AddressBook;
import com.gitlab.ajf.ccaddressbook.model.Contact;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class FileBackedStorageTest {

    private String contactStorageLocation;
    private String addressBookStorageLocation;

    @BeforeEach
    void setUp() {
        contactStorageLocation = "contact-" + UUID.randomUUID().toString() + ".json";
        addressBookStorageLocation = "addressBook-" + UUID.randomUUID().toString() + ".json";
    }

    @AfterEach
    void tearDown() throws IOException {
        Files.deleteIfExists(Path.of(contactStorageLocation));
        Files.deleteIfExists(Path.of(addressBookStorageLocation));
    }

    @Test
    void canSaveData() {
        assertFalse(Files.isRegularFile(Path.of(contactStorageLocation)));
        assertFalse(Files.isRegularFile(Path.of(addressBookStorageLocation)));

        FileBackedStorage<String, Contact> contactStorage = new FileBackedStorage<>(contactStorageLocation, new ObjectMapper(), String.class, Contact.class);
        FileBackedStorage<String, AddressBook> addressBookStorage = new FileBackedStorage<>(addressBookStorageLocation, new ObjectMapper(), String.class, AddressBook.class);

        contactStorage.saveData(Map.of("Jane", new Contact("Jane", "1234")));
        addressBookStorage.saveData(Map.of("Work", new AddressBook("Work", Set.of(new Contact("Jane", "1234")))));

        assertTrue(Files.isRegularFile(Path.of(contactStorageLocation)));
        assertTrue(Files.isRegularFile(Path.of(addressBookStorageLocation)));
    }

    @Test
    void canLoadContactData() throws URISyntaxException {
        String testFilePath = Paths.get(ClassLoader.getSystemResource("test_contact.json").toURI()).toAbsolutePath().toString();
        FileBackedStorage<String, Contact> storage = new FileBackedStorage<>(testFilePath, new ObjectMapper(), String.class, Contact.class);
        Optional<Map<String, Contact>> loadedData = storage.loadData();

        assertFalse(loadedData.isEmpty());
        assertEquals(1, loadedData.get().values().size());
    }

    @Test
    void canLoadAddressBookData() throws URISyntaxException {
        String testFilePath = Paths.get(ClassLoader.getSystemResource("test_addressBook.json").toURI()).toAbsolutePath().toString();
        FileBackedStorage<String, AddressBook> storage = new FileBackedStorage<>(testFilePath, new ObjectMapper(), String.class, AddressBook.class);
        Optional<Map<String, AddressBook>> loadedData = storage.loadData();

        assertFalse(loadedData.isEmpty());
        assertEquals(1, loadedData.get().values().size());
    }

    @Test
    void returnsAbsentWhenNoFile() {
        FileBackedStorage<String, AddressBook> storage = new FileBackedStorage<>(UUID.randomUUID().toString(), new ObjectMapper(), String.class, AddressBook.class);
        Optional<Map<String, AddressBook>> data = storage.loadData();

        assertTrue(data.isEmpty());
    }
}