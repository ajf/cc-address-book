package com.gitlab.ajf.ccaddressbook.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.ajf.ccaddressbook.exception.ContactNotFoundException;
import com.gitlab.ajf.ccaddressbook.model.Contact;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class FileBackedContactRepoTest {

    @Mock
    private ObjectMapper objectMapper;
    @Mock
    private FileBackedStorage<String, Contact> contactFileBackedStorage;
    @Captor
    private ArgumentCaptor<Map<String, Contact>> captor;

    private FileBackedContactRepo repo;

    @BeforeEach
    void setUp() {
        repo = new FileBackedContactRepo(contactFileBackedStorage);
    }

    @Test
    void shouldSaveAndRetrieveContact() {
        Contact frank = new Contact("Frank", "1234");
        repo.save(frank);

        verify(contactFileBackedStorage).saveData(captor.capture());

        Contact savedFrank = captor.getValue().get("Frank");
        assertNotNull(savedFrank);
        assertEquals(frank, savedFrank);
        assertEquals(frank.getPhoneNumber(), savedFrank.getPhoneNumber());
    }

    @Test
    void shouldFailLoadingContact() {
        assertThrows(ContactNotFoundException.class, () -> repo.get("doesNotExist"));
    }

    @Test
    void shouldUpdateExistingContact() {
        Contact frank = new Contact("Frank", "1234");
        repo.save(frank);
        // Save an initial Frank to the repo.
        Contact dbFrank = repo.get("Frank");
        assertEquals(frank.getPhoneNumber(), dbFrank.getPhoneNumber());
        // Change the phone number and save it.
        frank.setPhoneNumber("4321");
        repo.save(frank);
        // Get the new instance of frank from the repo.
        Contact newDbFrank = repo.get("Frank");
        assertEquals(frank, newDbFrank);
        assertEquals(frank.getPhoneNumber(), newDbFrank.getPhoneNumber());
        assertNotEquals(dbFrank.getPhoneNumber(), newDbFrank.getPhoneNumber());
    }

    @Test
    void shouldListAllContacts() {
        Contact frank = new Contact("Frank", "1234");
        Contact sue = new Contact("Sue", "4321");
        Contact chloe = new Contact("Chloe", "8239");

        repo.save(sue);
        repo.save(frank);
        repo.save(chloe);
        List<Contact> all = repo.getAll();

        assertEquals(chloe, all.get(0));
        assertEquals(frank, all.get(1));
        assertEquals(sue, all.get(2));
    }

    @Test
    void shouldLoadContactsFromFile() throws URISyntaxException {
        String testFilePath = Paths.get(ClassLoader.getSystemResource("test_data.json").toURI()).toAbsolutePath().toString();
        FileBackedContactRepo fileBackedContactRepo = new FileBackedContactRepo(new FileBackedStorage<>(testFilePath, new ObjectMapper(), String.class, Contact.class));

        Contact fred = fileBackedContactRepo.get("Fred");

        assertEquals("Fred", fred.getName());
        assertEquals("13579", fred.getPhoneNumber());
    }

}