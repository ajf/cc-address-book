package com.gitlab.ajf.ccaddressbook.controller;

import com.gitlab.ajf.ccaddressbook.model.AddressBook;
import com.gitlab.ajf.ccaddressbook.model.Contact;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {
        "storageLocation.addressBooks=test-addressBooks.json",
        "storageLocation.contacts=test-contacts.json"
})
class AddressBookControllerTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private String basePath;

    @BeforeEach
    void setUp() {
        basePath = "http://localhost:" + port + "/v1/address-book";
    }

    @AfterAll
    static void afterAll() throws IOException {
        Files.deleteIfExists(Paths.get("test-contacts.json"));
        Files.deleteIfExists(Paths.get("test-addressBooks.json"));
    }

    @Test
    void shouldSaveAndRetrieveAddressBook() {
        // 1. Submit an AddressBook
        Contact bob = new Contact("Bob", "9876");
        AddressBook work = new AddressBook("Work", Set.of(bob));
        restTemplate.postForEntity(basePath, work, AddressBook.class);
        // 2. Get the contact back
        AddressBook remoteWork = restTemplate.getForObject(basePath + "/" + work.getName(), AddressBook.class);

        assertEquals(work, remoteWork);
        assertEquals(work.getContacts(), remoteWork.getContacts());
    }

    @Test
    void shouldGetAllAddressBooks() {
        // 1. Submit multiple AddressBooks
        Contact bob = new Contact("Bob", "9876");
        AddressBook work = new AddressBook("Work", Set.of(bob));
        AddressBook home = new AddressBook("Home", Set.of(bob));

        restTemplate.postForEntity(basePath, work, AddressBook.class);
        restTemplate.postForEntity(basePath, home, AddressBook.class);
        // 2. Get all books
        ResponseEntity<AddressBook[]> response = restTemplate.getForEntity(basePath + "/",  AddressBook[].class);
        AddressBook[] addressBooks = response.getBody();
        // 3. Verify
        assertNotNull(addressBooks);
        assertEquals(2, addressBooks.length);
        assertTrue(Arrays.asList(addressBooks).contains(home));
        assertTrue(Arrays.asList(addressBooks).contains(work));
    }

    @Test
    void shouldGiveDisjunctionOfTwoSavedAddressBooks() {
        // 1. Submit multiple AddressBooks
        Contact sue = new Contact("Sue", "6430");
        Contact bob = new Contact("Bob", "9876");
        Contact zoe = new Contact("Zoe", "1234");
        Contact mary = new Contact("Mary", "1234");
        AddressBook work = new AddressBook("Work", Set.of(bob, mary, zoe));
        AddressBook home = new AddressBook("Home", Set.of(mary, sue, zoe));
        restTemplate.postForEntity(basePath, work, AddressBook.class);
        restTemplate.postForEntity(basePath, home, AddressBook.class);
        // 2. Get the disjunction books
        ResponseEntity<Contact[]> response = restTemplate.getForEntity(basePath + "/compare/Work/Home",  Contact[].class);
        Contact[] disjunction = response.getBody();
        // 3. Verify
        assertNotNull(disjunction);
        assertEquals(2, disjunction.length);
        assertTrue(Arrays.asList(disjunction).contains(sue));
        assertTrue(Arrays.asList(disjunction).contains(bob));
    }

    @Test
    void shouldGiveDisjunctionOfTwoProvidedAddressBooks() {
        // 1. Submit multiple AddressBooks
        Contact sue = new Contact("Sue", "6430");
        Contact bob = new Contact("Bob", "9876");
        Contact zoe = new Contact("Zoe", "1234");
        Contact mary = new Contact("Mary", "1234");
        AddressBook work = new AddressBook("Work", Set.of(bob, mary, zoe));
        AddressBook home = new AddressBook("Home", Set.of(mary, sue, zoe));
        List<AddressBook> books = List.of(work, home);
        // 2. Get the disjunction books
        ResponseEntity<Contact[]> responseEntity = restTemplate.postForEntity(basePath + "/compare", books, Contact[].class);
        Contact[] disjunction = responseEntity.getBody();
        // 3. Verify
        assertNotNull(disjunction);
        assertEquals(2, disjunction.length);
        assertTrue(Arrays.asList(disjunction).contains(sue));
        assertTrue(Arrays.asList(disjunction).contains(bob));
    }

    @Test
    void shouldRejectIncorrectlySizedRequests() {
        // 1. Submit multiple AddressBooks
        Contact sue = new Contact("Sue", "6430");
        Contact bob = new Contact("Bob", "9876");
        Contact zoe = new Contact("Zoe", "1234");
        Contact mary = new Contact("Mary", "1234");
        AddressBook work = new AddressBook("Work", Set.of(bob, mary, zoe));
        AddressBook home = new AddressBook("Home", Set.of(mary, sue, zoe));
        List<AddressBook> books = List.of(work, home, new AddressBook("school", null));
        // 2. Send too many!
        ResponseEntity<String> manyResponseEntity = restTemplate.postForEntity(basePath + "/compare", books, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, manyResponseEntity.getStatusCode());
        // Send too few!
        ResponseEntity<String> fewResponseEntity = restTemplate.postForEntity(basePath + "/compare", List.of(work), String.class);
        assertEquals(HttpStatus.BAD_REQUEST, fewResponseEntity.getStatusCode());
    }
}