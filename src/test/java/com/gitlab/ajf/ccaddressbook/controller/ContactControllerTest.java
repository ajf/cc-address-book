package com.gitlab.ajf.ccaddressbook.controller;

import com.gitlab.ajf.ccaddressbook.model.Contact;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * An alternative here would be to wire up the controller in question with mocks,
 * and test at that level.
 *
 * We could then do the API testing via something like pact.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {
        "storageLocation.contacts=test-contacts.json"
})
class ContactControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private String basePath;

    @BeforeEach
    void setUp() {
        basePath = "http://localhost:" + port + "/v1/contacts";
    }

    @AfterAll
    static void afterAll() throws IOException {
        Files.deleteIfExists(Paths.get("test-contacts.json"));
    }

    @Test
    void shouldSaveAndRetrieveAContact() {
        // 1. Submit a contact
        Contact bob = new Contact("Bob", "9876");
        restTemplate.postForEntity(basePath, bob, String.class);
        // 2. Get the contact back
        Contact remoteBob = restTemplate.getForObject(basePath + "/" + bob.getName(), Contact.class);

        assertEquals(bob, remoteBob);
        assertEquals(bob.getPhoneNumber(), remoteBob.getPhoneNumber());
    }

    @Test
    void shouldGetAllContacts() {
        // 1. Add multiple contacts
        Contact sue = new Contact("Sue", "6430");
        Contact bob = new Contact("Bob", "9876");
        Contact zoe = new Contact("Zoe", "1234");
        restTemplate.postForEntity(basePath, sue, String.class);
        restTemplate.postForEntity(basePath, bob, String.class);
        restTemplate.postForEntity(basePath, zoe, String.class);
        // 2. Get all contacts
        ResponseEntity<Contact[]> contacts = restTemplate.getForEntity(basePath + "/",  Contact[].class);
        Contact[] body = contacts.getBody();
        // 3. Verify that they're present, in order.
        assertNotNull(body);
        assertEquals(3, body.length);
        assertEquals(bob, body[0]);
        assertEquals(sue, body[1]);
        assertEquals(zoe, body[2]);
    }
}