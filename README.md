# Address Book Coding Challenge
[![pipeline status](https://gitlab.com/ajf/cc-address-book/badges/master/pipeline.svg)](https://gitlab.com/ajf/cc-address-book/-/commits/master)


## Requirements
Allow a user to store:
- Contacts which have:
    - A Name  
    - A Phone number
- Address books which have:
    - A Name
    - A set of Contact
    
Functionality:
- Display all contacts, sorted by name
- Compare two address books and provide the symmetric difference of the two address books. 

## Design
- Persist address books & contacts between executions
    - File based storage method, to avoid having an extra dependency of a database. 
- Provide a RESTful API for creating & listing contacts. 
    - /v1/contacts POST - add a contact 
    - /v1/contacts GET - get all contacts
    - /v1/contacts/{name} GET - get a specific contact
- Provide a RESTful API for address-book: 
    - /v1/address-book POST - create an address book, adding any contacts     
    - /v1/address-book GET - get all address books
    - /v1/address-book/{name} - get a specific address book 
    - /v1/address-book/compare/{book1}/{book2} GET - compare two saved address books, returning the symmetric difference/disjunction.  
    - /v1/address-book/compare POST - compare two provided address books, returning the symmetric difference/disjunction.  

## Assumptions
- Names are a unique identifier in this situation. Alternative would be to generate & assign an ID to each contact. 
- No validation on phone numbers, storing as a string to allow for both leading zeros and dialing prefixes. 
- Only one user will be accessing the service so no need for adding per-user address book storage.  
- Not dealing with any potential race conditions for writing data from multiple producers. Last one in wins. 

## CI/CD
I have CI/CD enabled for this repo because it's based off a template repo I have used in the past for personal projects, it makes life a lot easier. 

## Running the Code

### Java/Maven
Requirements: 
- Java 11 (Chosen as it is the current LTS version, might work on other versions)
As a Spring Boot maven application, the code can be compiled & run with: 

Linux & MacOS: 
```bash 
./mvnw spring-boot:run
```   

Windows, from command line (Not entirely sure here, I don't have a Windows machine to test on.)  
```
mvnw.cmd spring-boot:run
```

Data will be stored in `./storage` by default. This can be overridden by supplying custom storage locations at runtime: 

```bash
./mvnw -DstorageLocation.contacts=myCustomContacts.json -DstorageLocation.addressBooks=myCustomAddressBook.json spring-boot:run 
``` 

### Docker / docker-compose
Requirements:
- Docker on target host. 
- docker-compose (optional)

I have included a docker-compose file for easy running from the latest CI/CD build. This can be run from the project root using: 
```
docker-compose up
``` 

It will mount `./storage` as a working directory to store data in between application executions.

## Exploring the APIs
I have included Springfox for Swagger UI and Swagger API documentation, they can be found here when the application is running: 

- Swagger UI: http://localhost:8080/swagger-ui.html 
- Swagger API docs viewable at: http://localhost:8080/v2/api-docs

## Demonstrating functionality.
I have also included a demo bash script which runs through the required functionality using curl. It can be run from the project root by: 
```bash 
./demo.sh
``` 

