FROM openjdk:11-slim
# Normally would use alpine here, but: https://github.com/docker-library/openjdk/issues/211

VOLUME /storage/
COPY /target/*.jar /app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
EXPOSE 8080