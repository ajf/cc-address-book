#!/bin/bash

# Add an address book, which will also add the contacts in them, in this case: Bob, Mary, Jane & John.
echo "Saving a Work address book"
curl "http://localhost:8080/v1/address-book" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"name\": \"Work\", \"contacts\": [ { \"name\": \"Bob\", \"phoneNumber\": \"1234\"}, {\"name\": \"Mary\", \"phoneNumber\": \"2345\"}, {\"name\": \"Jane\", \"phoneNumber\": \"3456\" } ]}"
echo "Saving a Home address book"
curl "http://localhost:8080/v1/address-book" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"name\": \"Home\", \"contacts\": [ { \"name\": \"Mary\", \"phoneNumber\": \"2345\"}, {\"name\": \"John\", \"phoneNumber\": \"0987\"}, {\"name\": \"Jane\", \"phoneNumber\": \"3456\" } ]}"
# Get all the address books
echo "Getting all the address books"
curl -X GET "http://localhost:8080/v1/address-book" -H "accept: */*"
# Get all the contacts.
echo -e "\nGetting all contacts, names should be in alphabetical order. "
curl -X GET "http://localhost:8080/v1/contacts" -H "accept: */*"
# Get the disjunction between Work & Home
echo -e "\nGetting the disjunction between the Work address book, and the Home address book"
curl -X GET "http://localhost:8080/v1/address-book/compare/Work/Home" -H "accept: */*"